#include "Canvas.hpp"

Canvas::Canvas() {
  width = 800;
  height = 450;
  stroke = 0x000000;
  stroke_weight = 1;
  background_color = 0xffffff;
  for(int i = 0; i < 450; ++i) {
    std::vector<int> row;
    for(int j = 0; j < 800; ++j) {
      row.push_back(background_color);
    }
    pixels.push_back(row);
  }
}

Canvas::Canvas(int w, int h) {
  width = w;
  height = h;
  stroke = 0x000000;
  stroke_weight = 1;
  background_color = 0xffffff;
  for(int i = 0; i < h; ++i) {
    std::vector<int> row;
    for(int j = 0; j < w; ++j) {
      row.push_back(background_color);
    }
    pixels.push_back(row);
  }
}

std::vector<std::vector<int>> Canvas::get_pixels() {
  return pixels;
}

void Canvas::add_point(int x, int y) {
  pixels[y][x] = stroke;
}

void Canvas::add_point(int x, int y, int color) {
  pixels[y][x] = color;
}

void Canvas::add_line(int x1, int y1, int x2, int y2) {
  float m = 0;
  float 
  if(x2-x1 != 0) {
    m = (float)(y2 - y1) / (float)(x2 - x1);
  }

  float b = y1 - m*x1;
  float new_b = b + n;
  if(x2-x1 != 0) {
    for(int x = x1; x < std::abs(x2-x1)+1; ++x) {
      pixels[(int) m*x + b][x] = stroke;
    }
  } else {
    for(int y = x1; y < 0; ++y) {
      pixels[y][x1] = stroke;
    }
  }
}

void Canvas::set_background_color(int color) {
  background_color = color;
}