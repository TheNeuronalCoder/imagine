#include <vector>
#include <cmath>
#include <iostream>

class Canvas {
  private:
    std::vector<std::vector<int>> pixels;
  
  public:
    int width;
    int height;
    int stroke;
    int stroke_weight;
    int background_color;

    Canvas();
    Canvas(int, int);

    void add_point(int, int);
    void add_point(int, int, int);
    void add_line(int, int, int, int);
    std::vector<std::vector<int>> get_pixels();

    void set_background_color(int);
};